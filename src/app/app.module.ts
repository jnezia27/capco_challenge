import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyTableComponentComponent } from './my-table-component/my-table-component.component';
import { MyTableRowComponentComponent } from './my-table-row-component/my-table-row-component.component';

@NgModule({
  declarations: [
    AppComponent,
    MyTableComponentComponent,
    MyTableRowComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
