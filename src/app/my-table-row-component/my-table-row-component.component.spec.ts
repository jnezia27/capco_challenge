import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTableRowComponentComponent } from './my-table-row-component.component';

describe('MyTableRowComponentComponent', () => {
  let component: MyTableRowComponentComponent;
  let fixture: ComponentFixture<MyTableRowComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTableRowComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTableRowComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
